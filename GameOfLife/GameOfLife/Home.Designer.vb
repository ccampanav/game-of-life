﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Home
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Home))
        Me.PanelWorld = New System.Windows.Forms.Panel()
        Me.TrackBarSpeed = New System.Windows.Forms.TrackBar()
        Me.LabelSpeed = New System.Windows.Forms.Label()
        Me.TextBoxGenerations = New System.Windows.Forms.TextBox()
        Me.LabelGenerations = New System.Windows.Forms.Label()
        Me.LabelPosition = New System.Windows.Forms.Label()
        Me.ButtonSimulate = New System.Windows.Forms.Button()
        Me.ButtonLoad = New System.Windows.Forms.Button()
        Me.ButtonSave = New System.Windows.Forms.Button()
        Me.FDLoad = New System.Windows.Forms.OpenFileDialog()
        Me.FDSave = New System.Windows.Forms.SaveFileDialog()
        Me.ButtonClear = New System.Windows.Forms.Button()
        Me.RTBMap = New System.Windows.Forms.RichTextBox()
        CType(Me.TrackBarSpeed, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelWorld
        '
        Me.PanelWorld.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PanelWorld.BackColor = System.Drawing.Color.White
        Me.PanelWorld.Location = New System.Drawing.Point(2, -3)
        Me.PanelWorld.Margin = New System.Windows.Forms.Padding(0)
        Me.PanelWorld.Name = "PanelWorld"
        Me.PanelWorld.Size = New System.Drawing.Size(1200, 560)
        Me.PanelWorld.TabIndex = 0
        '
        'TrackBarSpeed
        '
        Me.TrackBarSpeed.AutoSize = False
        Me.TrackBarSpeed.Cursor = System.Windows.Forms.Cursors.Hand
        Me.TrackBarSpeed.Location = New System.Drawing.Point(680, 598)
        Me.TrackBarSpeed.Minimum = 1
        Me.TrackBarSpeed.Name = "TrackBarSpeed"
        Me.TrackBarSpeed.Size = New System.Drawing.Size(150, 32)
        Me.TrackBarSpeed.TabIndex = 4
        Me.TrackBarSpeed.Value = 1
        '
        'LabelSpeed
        '
        Me.LabelSpeed.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelSpeed.ForeColor = System.Drawing.Color.DimGray
        Me.LabelSpeed.Location = New System.Drawing.Point(700, 571)
        Me.LabelSpeed.Name = "LabelSpeed"
        Me.LabelSpeed.Size = New System.Drawing.Size(110, 20)
        Me.LabelSpeed.TabIndex = 5
        Me.LabelSpeed.Text = "Velocidad 1"
        Me.LabelSpeed.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'TextBoxGenerations
        '
        Me.TextBoxGenerations.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBoxGenerations.ForeColor = System.Drawing.Color.DimGray
        Me.TextBoxGenerations.Location = New System.Drawing.Point(515, 598)
        Me.TextBoxGenerations.MaxLength = 6
        Me.TextBoxGenerations.Name = "TextBoxGenerations"
        Me.TextBoxGenerations.Size = New System.Drawing.Size(80, 29)
        Me.TextBoxGenerations.TabIndex = 6
        Me.TextBoxGenerations.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'LabelGenerations
        '
        Me.LabelGenerations.AutoSize = True
        Me.LabelGenerations.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelGenerations.ForeColor = System.Drawing.Color.DimGray
        Me.LabelGenerations.Location = New System.Drawing.Point(505, 571)
        Me.LabelGenerations.Name = "LabelGenerations"
        Me.LabelGenerations.Size = New System.Drawing.Size(98, 20)
        Me.LabelGenerations.TabIndex = 7
        Me.LabelGenerations.Text = "Generaciones"
        Me.LabelGenerations.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'LabelPosition
        '
        Me.LabelPosition.AutoSize = True
        Me.LabelPosition.Font = New System.Drawing.Font("Segoe UI", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelPosition.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.LabelPosition.Location = New System.Drawing.Point(5, 569)
        Me.LabelPosition.Name = "LabelPosition"
        Me.LabelPosition.Size = New System.Drawing.Size(76, 20)
        Me.LabelPosition.TabIndex = 19
        Me.LabelPosition.Text = "Célula: 0,0"
        Me.LabelPosition.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'ButtonSimulate
        '
        Me.ButtonSimulate.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonSimulate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonSimulate.Font = New System.Drawing.Font("Segoe UI", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSimulate.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(176, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.ButtonSimulate.Location = New System.Drawing.Point(950, 574)
        Me.ButtonSimulate.Name = "ButtonSimulate"
        Me.ButtonSimulate.Size = New System.Drawing.Size(160, 50)
        Me.ButtonSimulate.TabIndex = 20
        Me.ButtonSimulate.Text = "Simular"
        Me.ButtonSimulate.UseVisualStyleBackColor = True
        '
        'ButtonLoad
        '
        Me.ButtonLoad.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonLoad.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonLoad.ForeColor = System.Drawing.Color.DimGray
        Me.ButtonLoad.Location = New System.Drawing.Point(186, 582)
        Me.ButtonLoad.Name = "ButtonLoad"
        Me.ButtonLoad.Size = New System.Drawing.Size(110, 36)
        Me.ButtonLoad.TabIndex = 21
        Me.ButtonLoad.Text = "Cargar"
        Me.ButtonLoad.UseVisualStyleBackColor = True
        '
        'ButtonSave
        '
        Me.ButtonSave.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonSave.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonSave.ForeColor = System.Drawing.Color.DimGray
        Me.ButtonSave.Location = New System.Drawing.Point(323, 582)
        Me.ButtonSave.Name = "ButtonSave"
        Me.ButtonSave.Size = New System.Drawing.Size(110, 36)
        Me.ButtonSave.TabIndex = 22
        Me.ButtonSave.Text = "Guardar mapa celular"
        Me.ButtonSave.UseVisualStyleBackColor = True
        '
        'FDLoad
        '
        Me.FDLoad.Filter = "Mapa celular |*.map"
        Me.FDLoad.Title = "Selecciona un mundo a cargar"
        '
        'FDSave
        '
        Me.FDSave.Filter = "Mapa celular |*.map"
        '
        'ButtonClear
        '
        Me.ButtonClear.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ButtonClear.FlatAppearance.BorderSize = 0
        Me.ButtonClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ButtonClear.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonClear.ForeColor = System.Drawing.Color.Red
        Me.ButtonClear.Location = New System.Drawing.Point(25, 603)
        Me.ButtonClear.Name = "ButtonClear"
        Me.ButtonClear.Size = New System.Drawing.Size(90, 30)
        Me.ButtonClear.TabIndex = 23
        Me.ButtonClear.Text = "Limpiar"
        Me.ButtonClear.UseVisualStyleBackColor = True
        '
        'RTBMap
        '
        Me.RTBMap.BackColor = System.Drawing.Color.Gainsboro
        Me.RTBMap.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RTBMap.Font = New System.Drawing.Font("Segoe UI", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RTBMap.ForeColor = System.Drawing.Color.DimGray
        Me.RTBMap.Location = New System.Drawing.Point(456, 610)
        Me.RTBMap.Name = "RTBMap"
        Me.RTBMap.ReadOnly = True
        Me.RTBMap.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None
        Me.RTBMap.Size = New System.Drawing.Size(40, 20)
        Me.RTBMap.TabIndex = 24
        Me.RTBMap.Text = ""
        Me.RTBMap.Visible = False
        '
        'Home
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 21.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1204, 641)
        Me.Controls.Add(Me.RTBMap)
        Me.Controls.Add(Me.ButtonClear)
        Me.Controls.Add(Me.ButtonSave)
        Me.Controls.Add(Me.ButtonLoad)
        Me.Controls.Add(Me.ButtonSimulate)
        Me.Controls.Add(Me.LabelPosition)
        Me.Controls.Add(Me.LabelGenerations)
        Me.Controls.Add(Me.TextBoxGenerations)
        Me.Controls.Add(Me.LabelSpeed)
        Me.Controls.Add(Me.TrackBarSpeed)
        Me.Controls.Add(Me.PanelWorld)
        Me.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.DimGray
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.MaximumSize = New System.Drawing.Size(1220, 680)
        Me.MinimumSize = New System.Drawing.Size(1220, 680)
        Me.Name = "Home"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Game of Life"
        CType(Me.TrackBarSpeed, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents PanelWorld As Panel
    Friend WithEvents TrackBarSpeed As TrackBar
    Friend WithEvents LabelSpeed As Label
    Friend WithEvents TextBoxGenerations As TextBox
    Friend WithEvents LabelGenerations As Label
    Friend WithEvents LabelPosition As Label
    Friend WithEvents ButtonSimulate As Button
    Friend WithEvents ButtonLoad As Button
    Friend WithEvents ButtonSave As Button
    Friend WithEvents FDLoad As OpenFileDialog
    Friend WithEvents FDSave As SaveFileDialog
    Friend WithEvents ButtonClear As Button
    Friend WithEvents RTBMap As RichTextBox
End Class
