﻿Imports System.IO
Public Class Home
    Dim world(75, 35) As Boolean

    Private Sub Home_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call GenerateWorld(True)
    End Sub

    Private Sub GenerateWorld(newWorld As Boolean)
        PanelWorld.Controls.Clear()
        For y = 1 To 35 Step 1
            For x = 1 To 75 Step 1
                Dim newCell As New PictureBox : newCell.Name = "cell" & x & "," & y
                newCell.Width = 16 : newCell.Height = 16 : newCell.BackColor = Color.White
                newCell.BackgroundImageLayout = ImageLayout.Stretch
                newCell.SizeMode = PictureBoxSizeMode.StretchImage
                newCell.Left = (x - 1) * 16 : newCell.Top = (y - 1) * 16
                newCell.BorderStyle = BorderStyle.FixedSingle : newCell.Cursor = Cursors.Hand
                If newWorld = True Then
                    world(x, y) = False
                    newCell.BackgroundImage = Nothing
                Else
                    If world(x, y) = True Then
                        newCell.BackgroundImage = My.Resources.cell
                    Else
                        newCell.BackgroundImage = Nothing
                    End If
                End If
                AddHandler newCell.MouseEnter, AddressOf newCell_MouseEnter
                AddHandler newCell.MouseLeave, AddressOf newCell_MouseLeave
                AddHandler newCell.MouseClick, AddressOf newCell_MouseClick
                PanelWorld.Controls.Add(newCell)
            Next
        Next
    End Sub

    Private Sub newCell_MouseEnter(ByVal sender As Object, ByVal e As EventArgs)
        LabelPosition.Text = "Célula: " & Mid(CType(sender, PictureBox).Name, 5)
        CType(sender, PictureBox).Image = My.Resources.cellSelected
    End Sub

    Private Sub newCell_MouseLeave(ByVal sender As Object, ByVal e As EventArgs)
        CType(sender, PictureBox).Image = Nothing
    End Sub

    Private Sub newCell_MouseClick(ByVal sender As Object, ByVal e As EventArgs)
        Dim x, y As Byte
        Dim id As String = Mid(CType(sender, PictureBox).Name, 5)
        For i = 1 To Len(id) Step 1
            If Mid(id, i, 1).Equals(",") = True Then
                x = CByte(Mid(id, 1, i - 1)) : y = CByte(Mid(id, i + 1)) : Exit For
            End If
        Next
        If world(x, y) = True Then
            world(x, y) = False : CType(sender, PictureBox).BackgroundImage = Nothing
        Else
            world(x, y) = True : CType(sender, PictureBox).BackgroundImage = My.Resources.cell
        End If
    End Sub

    Private Sub TextBoxGenerations_TextChanged(sender As Object, e As EventArgs) Handles TextBoxGenerations.TextChanged
        If Len(TextBoxGenerations.Text) > 6 Then
            TextBoxGenerations.Text = Mid(TextBoxGenerations.Text, 1, 6)
        End If
        For i = 1 To Len(TextBoxGenerations.Text) Step 1
            If InStr("1234567890", Mid(TextBoxGenerations.Text, i, 1), CompareMethod.Binary) = 0 Then
                TextBoxGenerations.Text = Replace(TextBoxGenerations.Text, Mid(TextBoxGenerations.Text, i, 1), "")
                TextBoxGenerations.SelectionStart = TextBoxGenerations.TextLength
            End If
        Next
    End Sub

    Private Sub TrackBarSpeed_ValueChanged(sender As Object, e As EventArgs) Handles TrackBarSpeed.ValueChanged
        LabelSpeed.Text = "Velocidad " & TrackBarSpeed.Value
    End Sub
    Private Sub ButtonLoad_Click(sender As Object, e As EventArgs) Handles ButtonLoad.Click
        FDLoad.InitialDirectory = My.Application.Info.DirectoryPath
        If FDLoad.ShowDialog() = DialogResult.OK Then
            RTBMap.Clear()
            RTBMap.LoadFile(Path.GetFullPath(FDLoad.FileName), fileType:=RichTextBoxStreamType.PlainText)
            Call LoadMap()
        End If
    End Sub

    Private Sub LoadMap()
        Try
            Dim speed As Byte
            Dim generations As Integer
            Dim line As String = ""
            Dim x, y As Short
            For i = 1 To Len(RTBMap.Lines(0)) Step 1
                If Mid(RTBMap.Lines(0), i, 1).Equals(":") = True Then
                    speed = CByte(Mid(RTBMap.Lines(0), 1, i - 1))
                    generations = CInt(Mid(RTBMap.Lines(0), i + 1))
                    Exit For
                End If
            Next
            TextBoxGenerations.Text = generations : TrackBarSpeed.Value = speed
            y = 1
            For i = 1 To RTBMap.Lines.Count - 1 Step 1
                x = 0
                For j = 1 To Len(RTBMap.Lines(i)) Step 1
                    If j Mod 2 = 1 Then
                        x += 1
                        If Mid(RTBMap.Lines(i), j, 1).Equals("0") = True Then
                            world(x, y) = False
                        End If
                        If Mid(RTBMap.Lines(i), j, 1).Equals("1") = True Then
                            world(x, y) = True
                        End If
                    End If
                Next
                y += 1
            Next
            Call GenerateWorld(False)
            MsgBox("Mapa celular cargado", MsgBoxStyle.Information, "Game of Life")
        Catch ex As Exception
            MsgBox("Mapa celular dañado", MsgBoxStyle.Exclamation, "Game of Life")
        End Try
    End Sub

    Private Sub ButtonSave_Click(sender As Object, e As EventArgs) Handles ButtonSave.Click
        If Len(TextBoxGenerations.Text) > 0 Then
            If CInt(TextBoxGenerations.Text) > 0 Then
                FDSave.InitialDirectory = My.Application.Info.DirectoryPath
                If FDSave.ShowDialog() = DialogResult.OK Then
                    Dim strPathFD As String = FDSave.FileName
                    strPathFD = strPathFD.ToLower
                    Dim strPathIncorrect As String = My.Application.Info.DirectoryPath & "\simulate.map"
                    strPathIncorrect = strPathIncorrect.ToLower
                    If strPathFD.Equals(strPathIncorrect) = True Then
                        MsgBox("Nombre de mapa inválido", MsgBoxStyle.Exclamation, "Game of Life")
                    Else
                        Call SaveMap(Path.GetFullPath(FDSave.FileName))
                        MsgBox("Mapa celular guardado", MsgBoxStyle.Information, "Game of Life")
                    End If
                End If
            Else
                MsgBox("Máximo de generaciones incorrecto", MsgBoxStyle.Exclamation, "Game of Life")
            End If
        Else
            MsgBox("Ingresa un máximo de generaciones", MsgBoxStyle.Exclamation, "Game of Life")
        End If
    End Sub

    Private Sub SaveMap(strMap As String)
        Dim strLine As String = TrackBarSpeed.Value & ":" & TextBoxGenerations.Text
        My.Computer.FileSystem.WriteAllText(strMap, strLine, False)
        For y = 1 To 35 Step 1
            strLine &= vbCrLf
            For x = 1 To 75 Step 1
                If world(x, y) = True Then
                    strLine &= "1 "
                Else
                    strLine &= "0 "
                End If
            Next
        Next
        My.Computer.FileSystem.WriteAllText(strMap, strLine, False)
    End Sub

    Private Sub ButtonClear_Click(sender As Object, e As EventArgs) Handles ButtonClear.Click
        Dim answ As MsgBoxResult = MsgBox("Confirmar limpieza de mapa", MsgBoxStyle.YesNo, "Game of Life")
        If answ = MsgBoxResult.Yes Then
            Me.Enabled = False
            TextBoxGenerations.Text = ""
            TrackBarSpeed.Value = 1
            Call GenerateWorld(True)
            Me.Enabled = True
        End If
    End Sub

    Private Sub ButtonSimulate_Click(sender As Object, e As EventArgs) Handles ButtonSimulate.Click
        If My.Computer.FileSystem.FileExists("Simulator.exe") Then
            Call SaveToSimulate()
            Process.Start("Simulator.exe")
        Else
            MsgBox("No existe el simulador", MsgBoxStyle.Critical, "Game of Life")
        End If
    End Sub

    Private Sub SaveToSimulate()
        Dim sPath As String = My.Application.Info.DirectoryPath & "\simulate.map"
        Dim strLine As String = TrackBarSpeed.Value & ":" & TextBoxGenerations.Text
        My.Computer.FileSystem.WriteAllText(sPath, strLine, False)
        For y = 1 To 35 Step 1
            strLine &= vbCrLf
            For x = 1 To 75 Step 1
                If world(x, y) = True Then
                    strLine &= "1 "
                Else
                    strLine &= "0 "
                End If
            Next
        Next
        My.Computer.FileSystem.WriteAllText(sPath, strLine, False)
    End Sub

End Class
