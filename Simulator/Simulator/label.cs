﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Simulator
{
    class label
    {
        SpriteFont font;
        Vector2 position;
        string text;

        public label(ContentManager c, string txt, float pX, float pY)
        {
            this.text = txt;
            font = c.Load<SpriteFont>("fontLabel");
            position = new Vector2(pX, pY);
        }

        public void setText(string txt)
        {
            this.text = txt;
        }

        public void draw(SpriteBatch sprite)
        {
            sprite.DrawString(font, text, position, Color.DimGray);
        }
    }
}
