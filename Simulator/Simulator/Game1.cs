﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using System.IO;
using System.Windows.Forms;
using System;

namespace Simulator
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        bool[,] map = new bool[76, 36];
        cell[,] world = new cell[76,36];
        byte speed;
        ushort generations;
        label txtGenerations, txtTime, txtLivingCell, txtTotalCell, txtPopulation;
        //######### Var RunTime #########
        ushort nowGeneration, timeLimit, livingCell;
        float population;
        UInt32 totalCell;
        byte neighbors;
        bool[,] neigh = new bool[4, 4];
        bool finish;
        //###############################
        ushort nowSecond, genSecond = 0;
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1200;
            graphics.PreferredBackBufferHeight = 610;
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\simulate.map") == false)
            {
                MessageBox.Show("Error al iniciar el simulador.");
                EndDraw();
            }
            StreamReader sr = new StreamReader(AppDomain.CurrentDomain.BaseDirectory + "\\simulate.map");
            string line = sr.ReadLine(); 
            speed = byte.Parse(line.Substring(0, line.IndexOf(":")));
            generations = ushort.Parse(line.Substring(line.IndexOf(":") + 1));
            switch (speed)
            {
                case 1:
                    timeLimit = 900; break;
                case 2:
                    timeLimit = 800; break;
                case 3:
                    timeLimit = 700; break;
                case 4:
                    timeLimit = 600; break;
                case 5:
                    timeLimit = 500; break;
                case 6:
                    timeLimit = 400; break;
                case 7:
                    timeLimit = 300; break;
                case 8:
                    timeLimit = 200; break;
                case 9:
                    timeLimit = 100; break;
                case 10:
                    timeLimit = 2; break;
                default:
                    MessageBox.Show("Error en la velocidad.");
                    Exit(); break;
            }
            totalCell = 0;
            short x, y=1;
            while ((line=sr.ReadLine()) != null)
            {
                x = 0;
                for (short l = 0; l<=line.Length-2; l+=2)
                {
                    x++;             
                    if (line.Substring(l, 1).Equals("0") == true)
                    {
                        map[x, y] = false;                    
                    }
                    if (line.Substring(l, 1).Equals("1") == true)
                    {
                        map[x, y] = true; totalCell++;
                    }
                }
                y++;
            }
            short cont = 0, pX, pY;
            byte st;
            for (y=1; y<=35; y++)
            {
                for (x=1; x<=75; x++)
                {
                    cont++;
                    if (map[x, y] == true)
                    {
                        st = 1;
                    }
                    else
                    {
                        st = 0;
                    }
                    pX = (short)((x-1)*16); pY = (short)((y-1)*16);
                    world[x, y] = new cell(Content, "cell" + cont, st, pX, pY);
                }
            }
            nowGeneration = 0;
            livingCell = (ushort)totalCell;
            population = (livingCell * 100f) / 2625f;
            txtGenerations = new label(Content, "Generaciones: " + nowGeneration + "/" + generations, 20f, 564f);
            txtTime = new label(Content, "Tiempo: 00:00:00.0", 400f, 564f);
            txtLivingCell = new label(Content, "Celulas vivas: " + livingCell, 20f, 580f);
            txtTotalCell = new label(Content, "Celulas generadas: " + totalCell, 400f, 580f);
            txtPopulation = new label(Content, "Poblacion: " + population + " %", 800f, 580f);           
            finish = false;
            base.Initialize();
        }

        protected override void LoadContent()
        {         
            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void UnloadContent()
        {         
        }

        protected override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Escape))
            {
                Exit();
            }
            nowSecond = (ushort)gameTime.TotalGameTime.TotalSeconds;
            //if (Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Space) && finish==false && nowSecond != genSecond)
            if (((int) gameTime.TotalGameTime.TotalMilliseconds % timeLimit == 0) && finish ==false)
            {
                genSecond = nowSecond;
                nowGeneration++;
                if (nowGeneration > 0)
                {
                    if (nowGeneration <= generations)
                    {
                        //#################### Life ####################
                        for (short y = 1; y <= 35; y++)
                        {
                            for (short x = 1; x <= 75; x++)
                            {
                                if (world[x, y].status == 1)
                                {
                                    neighbors = contNeighbors(x, y);
                                    if (neighbors == 2 || neighbors == 3) //Survival
                                    {
                                        world[x, y].status = 4; //Immune
                                    }
                                }
                            }
                        }
                        for (short y = 1; y <= 35; y++)
                        {
                            for (short x = 1; x <= 75; x++)
                            {
                                if (world[x, y].status == 1)
                                {
                                    world[x, y].status = 2; //To Death
                                }
                                if (world[x, y].status == 4)
                                {
                                    world[x, y].status = 1; //Remove immnue
                                }
                            }
                        }
                        for (short y = 1; y <= 35; y++)
                        {
                            for (short x = 1; x <= 75; x++)
                            {
                                if (world[x, y].status == 0)
                                {
                                    neighbors = getNeighbors(x, y, 0);
                                    if (neighbors == 3) //To Birth
                                    {
                                        world[x, y].status = 5;      
                                    }
                                }
                            }
                        }
                        for (short y = 1; y <= 35; y++)
                        {
                            for (short x = 1; x <= 75; x++)
                            {
                                if (world[x, y].status == 5) //Birth
                                {
                                    world[x, y].status = 1;
                                    totalCell++;
                                }
                            }
                        }
                        //##########################################
                        livingCell = 0;
                        for (short y = 1; y <= 35; y++)
                        {
                            for (short x = 1; x <= 75; x++)
                            {
                                if (world[x, y].status == 2 || world[x, y].status == 3)
                                {
                                    world[x, y].status = 0;
                                }
                                if (world[x,y].status == 1)
                                {
                                    livingCell++;
                                }
                            }
                        }
                        population = (livingCell * 100f) / 2625f;
                        txtGenerations.setText("Generaciones: " + nowGeneration + "/" + generations);              
                        txtLivingCell.setText("Celulas vivas: " + livingCell);
                        txtTotalCell.setText("Celulas generadas: " + totalCell);
                        txtPopulation.setText("Poblacion: " + population + " %");
                        if (livingCell == 0)
                        {
                            finish = true;
                        }
                    }
                    else
                    {
                        finish = true; MessageBox.Show("Simulación finalizada");
                    }
                }             
            }
            if (finish == false)
            {
                txtTime.setText("Tiempo: " + gameTime.TotalGameTime.ToString());
            }          
            base.Update(gameTime);
        }

        private void getNeighborsAllowed(int x, int y)
        {
            for (short yy = 1; yy <= 3; yy++)
            {
                for (short xx = 1; xx <= 3; xx++)
                {
                    neigh[xx, yy] = true;
                }
            }
            neigh[2, 2] = false;
            if (x == 1)
            {
                for (short ii = 1; ii <= 3; ii++)
                {
                    neigh[1, ii] = false;
                }
            }
            if (x == 75)
            {
                for (short ii = 1; ii <= 3; ii++)
                {
                    neigh[3, ii] = false;
                }
            }
            if (y == 1)
            {
                for (short ii = 1; ii <= 3; ii++)
                {
                    neigh[ii, 1] = false;
                }
            }
            if (y == 35)
            {
                for (short ii = 1; ii <= 3; ii++)
                {
                    neigh[ii, 3] = false;
                }
            }
        }

        private byte contNeighbors(int x, int y)
        {
            byte cont = 0;
            getNeighborsAllowed(x,y);            
            if (neigh[1, 1] == true)
            {
                if (world[x - 1, y - 1].status == 1 || world[x - 1, y - 1].status == 4)
                {
                    cont++;
                }
            }
            if (neigh[2, 1] == true)
            {
                if (world[x, y - 1].status == 1 || world[x, y - 1].status == 4)
                {
                    cont++;
                }
            }
            if (neigh[3, 1] == true)
            {
                if (world[x + 1, y - 1].status == 1 || world[x + 1, y - 1].status == 4)
                {
                    cont++;
                }
            }
            if (neigh[1, 2] == true)
            {
                if (world[x - 1, y].status == 1 || world[x - 1, y].status == 4)
                {
                    cont++;
                }
            }
            if (neigh[3, 2] == true)
            {
                if (world[x + 1, y].status == 1 || world[x + 1, y].status == 4)
                {
                    cont++;
                }
            }
            if (neigh[1, 3] == true)
            {
                if (world[x - 1, y + 1].status == 1 || world[x - 1, y + 1].status == 4)
                {
                    cont++;
                }
            }
            if (neigh[2, 3] == true)
            {
                if (world[x, y + 1].status == 1 || world[x, y + 1].status == 4)
                {
                    cont++;
                }
            }
            if (neigh[3, 3] == true)
            {
                if (world[x + 1, y + 1].status == 1 || world[x + 1, y + 1].status == 4)
                {
                    cont++;
                }
            }
            return cont;
        }

        private byte getNeighbors(int x, int y, byte st)
        {
            byte cont = 0;
            getNeighborsAllowed(x, y);
            if (st == 1)
            {
                if (neigh[1, 1] == true)
                {
                    if (world[x - 1, y - 1].status == 1)
                    {
                        cont++;
                    }
                }
                if (neigh[2, 1] == true)
                {
                    if (world[x, y - 1].status == 1)
                    {
                        cont++;
                    }
                }
                if (neigh[3, 1] == true)
                {
                    if (world[x + 1, y - 1].status == 1)
                    {
                        cont++;
                    }
                }
                if (neigh[1, 2] == true)
                {
                    if (world[x - 1, y].status == 1)
                    {
                        cont++;
                    }
                }
                if (neigh[3, 2] == true)
                {
                    if (world[x + 1, y].status == 1)
                    {
                        cont++;
                    }
                }
                if (neigh[1, 3] == true)
                {
                    if (world[x - 1, y + 1].status == 1)
                    {
                        cont++;
                    }
                }
                if (neigh[2, 3] == true)
                {
                    if (world[x, y + 1].status == 1)
                    {
                        cont++;
                    }
                }
                if (neigh[3, 3] == true)
                {
                    if (world[x + 1, y + 1].status == 1)
                    {
                        cont++;
                    }
                }
            }
            if (st == 0)
            {
                if (neigh[1, 1] == true)
                {
                    if (world[x - 1, y - 1].status == 1 || world[x - 1, y - 1].status == 2)
                    {
                        cont++;
                    }
                }
                if (neigh[2, 1] == true)
                {
                    if (world[x, y - 1].status == 1 || world[x, y - 1].status == 2)
                    {
                        cont++;
                    }
                }
                if (neigh[3, 1] == true)
                {
                    if (world[x + 1, y - 1].status == 1 || world[x + 1, y - 1].status == 2)
                    {
                        cont++;
                    }
                }
                if (neigh[1, 2] == true)
                {
                    if (world[x - 1, y].status == 1 || world[x - 1, y].status == 2)
                    {
                        cont++;
                    }
                }
                if (neigh[3, 2] == true)
                {
                    if (world[x + 1, y].status == 1 || world[x + 1, y].status == 2)
                    {
                        cont++;
                    }
                }
                if (neigh[1, 3] == true)
                {
                    if (world[x - 1, y + 1].status == 1 || world[x - 1, y + 1].status == 2)
                    {
                        cont++;
                    }
                }
                if (neigh[2, 3] == true)
                {
                    if (world[x, y + 1].status == 1 || world[x, y + 1].status == 2)
                    {
                        cont++;
                    }
                }
                if (neigh[3, 3] == true)
                {
                    if (world[x + 1, y + 1].status == 1 || world[x + 1, y + 1].status == 2)
                    {
                        cont++;
                    }
                }
            }
            return cont;
        }

        private void afterBirth(int x, int y)
        {
            getNeighborsAllowed(x, y);
            if (neigh[1, 1] == true)
            {
                if (world[x - 1, y - 1].status == 0)
                {
                    world[x - 1, y - 1].status = 3;
                }
            }
            if (neigh[2, 1] == true)
            {
                if (world[x, y - 1].status == 0)
                {
                    world[x, y - 1].status = 3;
                }
            }
            if (neigh[3, 1] == true)
            {
                if (world[x + 1, y - 1].status == 0)
                {
                    world[x + 1, y - 1].status = 3;
                }
            }
            if (neigh[1, 2] == true)
            {
                if (world[x - 1, y].status == 0)
                {
                    world[x - 1, y].status = 3;
                }
            }
            if (neigh[3, 2] == true)
            {
                if (world[x + 1, y].status == 0)
                {
                    world[x + 1, y].status = 3;
                }
            }
            if (neigh[1, 3] == true)
            {
                if (world[x - 1, y + 1].status == 0)
                {
                    world[x - 1, y + 1].status = 3;
                }
            }
            if (neigh[2, 3] == true)
            {
                if (world[x, y + 1].status == 0)
                {
                    world[x, y + 1].status = 3;
                }
            }
            if (neigh[3, 3] == true)
            {
                if (world[x + 1, y + 1].status == 0)
                {
                    world[x + 1, y + 1].status = 3;
                }
            }
        }

        protected override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            GraphicsDevice.Clear(Color.White);
            for (short y = 1; y <= 35; y++)
            {
                for (short x = 1; x <= 75; x++)
                {
                    world[x, y].draw(spriteBatch);
                }
            }
            txtGenerations.draw(spriteBatch);
            txtTime.draw(spriteBatch);
            txtLivingCell.draw(spriteBatch);
            txtTotalCell.draw(spriteBatch);
            txtPopulation.draw(spriteBatch);
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
