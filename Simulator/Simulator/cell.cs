﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Simulator
{
    class cell
    {
        Texture2D none, image;
        Rectangle rect;
        string name;
        short width, height;
        public byte status;

        public cell(ContentManager c, string n, byte st, short pX, short pY)
        {
            //0=Death 1=Life 2=ToDeath 3=Busy 4=Immnue 5=ToBirth
            this.name = n; this.status = st;
            width = 16; height = 16;
            rect = new Rectangle(pX, pY, width, height);
            none = c.Load<Texture2D>("img_none");
            image = c.Load<Texture2D>("img_cell");
        }

        public void draw(SpriteBatch sprite)
        {
            switch (status)
            {
                case 0:
                    sprite.Draw(none, rect, Color.White);
                    break;
                case 1:
                    sprite.Draw(image, rect, Color.White);
                    break;
            }
        }

    }
}
